﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RestApiTest.Models
{
    public class InvoicesDto
    {
        public Guid Id { get; set; }
        public string InvoiceNumber { get; set; }
        public string Code { get; set; }
        public DateTime Date { get; set; }
    }
}
