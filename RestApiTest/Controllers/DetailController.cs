﻿using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using RestApiTest.Models;
using RestApiTest.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RestApiTest.Controllers
{
    [Route("api/invoices/{invoiceId}/details")]
    public class DetailController : Controller
    {
        private IInvoiceRepository _invoiceRepository;

        public DetailController(IInvoiceRepository invoiceRepository)
        {
            _invoiceRepository = invoiceRepository;
        }

        [HttpGet()]
        public IActionResult GetDetailForInvoice(Guid invoiceId)
        {
            if (!_invoiceRepository.InvoiceExists(invoiceId))
            {
                return NotFound();
            }

            var detailsFromRepo = _invoiceRepository.GetDetailsForInvoice(invoiceId);

            var details = Mapper.Map<IEnumerable<DetialDto>>(detailsFromRepo);

            return Ok(details);
        }
        [HttpGet("{detailId}")]
        public IActionResult GetDetailForInvoice(Guid invoiceId,Guid detailId)
        {
            if (!_invoiceRepository.InvoiceExists(invoiceId))
            {
                return NotFound();
            }

            var detailFromRepo = _invoiceRepository.GetDetailForInvoice(invoiceId, detailId);

            if(detailFromRepo == null)
            {
                return NotFound();
            }

            var detail = Mapper.Map<DetialDto>(detailFromRepo);

            return Ok(detail);
        }
    }
}
