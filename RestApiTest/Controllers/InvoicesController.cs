﻿using Microsoft.AspNetCore.Mvc;
using RestApiTest.Models;
using RestApiTest.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RestApiTest.Controllers
{
    [Route("api/invoices")]
    public class InvoicesController : Controller
    {
        private IInvoiceRepository _invoiceRepository;

        public InvoicesController(IInvoiceRepository invoiceRepository)
        {
            _invoiceRepository = invoiceRepository;
        }
        [HttpGet]
        public IActionResult GetInvoices()
        {
           
            var invoicesFromRepo = _invoiceRepository.GetInvoices();
            var invoices = AutoMapper.Mapper.Map<IEnumerable<InvoicesDto>>(invoicesFromRepo);

            return Ok(invoices);
        }
        [HttpGet("{invoiceId}")]
        public IActionResult GetInvoice(Guid invoiceId)
        {

            var invoiceFromRepo = _invoiceRepository.GetInvoice(invoiceId);

            if (invoiceFromRepo == null)
            {
                return NotFound();
            }

            var invoice = AutoMapper.Mapper.Map<InvoicesDto>(invoiceFromRepo);

            return Ok(invoice);
        }

    }
}
