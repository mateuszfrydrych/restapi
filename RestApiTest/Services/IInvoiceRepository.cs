﻿using RestApiTest.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RestApiTest.Services
{
    public interface IInvoiceRepository
    {
        IEnumerable<Invoice> GetInvoices();
        Invoice GetInvoice(Guid invoiceID);
        void AddInvoice(Invoice invoice);
        void DeleteInvoice(Invoice invoice);
        void UpdateInvoice(Invoice invoice);
        bool InvoiceExists(Guid invoiceId);
        IEnumerable<Detail> GetDetailsForInvoice(Guid Invoice);
        Detail GetDetailForInvoice(Guid invoiceId, Guid detailId);
        void AddDetailFotInvoice(Guid invoiceId, Detail detail);
        void UpdateDetailForInvoice(Detail detail);
        void DeleteDetail(Detail detail);
        bool Save();
    }
}
