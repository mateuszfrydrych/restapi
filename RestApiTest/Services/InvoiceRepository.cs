﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using RestApiTest.Entities;
using Microsoft.EntityFrameworkCore;

namespace RestApiTest.Services
{
    public class InvoiceRepository : IInvoiceRepository
    {
        private InvoiceContext _context;

        public InvoiceRepository(InvoiceContext context)
        {
            _context = context;
        }

        public void AddDetailFotInvoice(Guid invoiceId, Detail detail)
        {
            throw new NotImplementedException();
        }

        public void AddInvoice(Invoice invoice)
        {
            invoice.Id = Guid.NewGuid();
            _context.Invoices.Add(invoice);

            if (invoice.Details.Any())
            {
                foreach (var detail in invoice.Details)
                {
                    _context.Details.Add(detail);
                }
            }
            _context.SaveChanges();
        }

        public void DeleteDetail(Detail detail)
        {
            throw new NotImplementedException();
        }

        public void DeleteInvoice(Invoice invoice)
        {
            throw new NotImplementedException();
        }

        public Detail GetDetailForInvoice(Guid invoiceId, Guid detailId)
        {
            return _context.Details.Where(x => x.InvoiceId == invoiceId).FirstOrDefault();
        }

        public IEnumerable<Detail> GetDetailsForInvoice(Guid invoiceId)
        {
            return _context.Details.Where(d => d.InvoiceId == invoiceId).ToList();
        }

        public Invoice GetInvoice(Guid invoiceID)
        {
            return _context.Invoices.Where(i => i.Id == invoiceID).FirstOrDefault();
        }

        public IEnumerable<Invoice> GetInvoices()
        {
            return _context.Invoices.ToList();
        }

        public bool InvoiceExists(Guid invoiceId)
        {
            if(_context.Invoices.Where(i=>i.Id == invoiceId).FirstOrDefault() == null)
            {
                return false;
            }
            return true;
        }

        public bool Save()
        {
            throw new NotImplementedException();
        }

        public void UpdateDetailForInvoice(Detail detail)
        {
            throw new NotImplementedException();
        }

        public void UpdateInvoice(Invoice invoice)
        {
            throw new NotImplementedException();
        }
    }
}
