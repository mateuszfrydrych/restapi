﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RestApiTest.Entities
{
    public static class InvoiceContextExtension
    {
        public static void SeedData(this InvoiceContext context)
        {
            context.Invoices.RemoveRange(context.Invoices);
            context.SaveChanges();

            var invoices = new List<Invoice>();
            invoices.Add(
                new Invoice()
                {
                    Id = new Guid("12345678-1234-1234-1234-123456789123"),
                    InvoiceNumber = "FN123231",
                    Code = "SALE",
                    Date = Convert.ToDateTime("2018-02-12"),
                    Details = new List<Detail>()
                    {
                        new Detail()
                        {
                            Id= new Guid("22345678-1234-1234-1234-123456789123"),
                            Type = "SALE",
                            Value = 432.32M
                        },
                        new Detail()
                        {
                            Id= new Guid(),
                            Type = "SALE",
                            Value = 4323.32M
                        },
                        new Detail()
                        {
                            Id= new Guid(),
                            Type = "SALE",
                            Value = 43.32M
                        }
                    }

                });

            for (int i = 0; i < 100; i++)
            {
                invoices.Add(
                new Invoice()
                {
                    Id = new Guid(),
                    InvoiceNumber = "FN123231",
                    Code = "SALE",
                    Date = Convert.ToDateTime("2018-02-12"),
                    Details = new List<Detail>()
                    {
                        new Detail()
                        {
                            Id= new Guid(),
                            Type = "SALE",
                            Value = 432.32M
                        },
                        new Detail()
                        {
                            Id= new Guid(),
                            Type = "SALE",
                            Value = 4323.32M
                        },
                        new Detail()
                        {
                            Id= new Guid(),
                            Type = "SALE",
                            Value = 43.32M
                        }
                    }

                });
            }

            foreach (var invoice in invoices)
            {
                context.Invoices.Add(invoice);
            }
            context.SaveChanges();
        }
    }
}
