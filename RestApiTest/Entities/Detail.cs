﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace RestApiTest.Entities
{
    public class Detail
    {
        [Key]
        public Guid Id { get; set; }

        [Required]
        [MaxLength(50)]
        public string Type { get; set; }

        [Required]
        public decimal Value { get; set; }

        [ForeignKey("InvoiceId")]
        public Invoice Invoice { get; set; }

        public Guid InvoiceId { get; set; }
    }
}
