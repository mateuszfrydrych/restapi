﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace RestApiTest.Entities
{
    public class Invoice
    {
        [Key]
        public Guid Id { get; set; }

        [Required]
        [MaxLength(50)]
        public string InvoiceNumber { get; set; }

        [Required]
        [MaxLength(5)]
        public string Code { get; set; }

        [Required]
        public DateTime Date { get; set; }

        public ICollection<Detail> Details { get; set; }
            = new List<Detail>();

    }
}
